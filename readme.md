# jb4jupyter

Out of this world beautiful plots with dark mode for earth!

Dark mode saves power. Saving power keeps more fossil fuels in the ground.  More fossil fuels in the ground means we are saving earth.

## usage


## notes for deploying:
to make this package you need to do some things:
0. make sure the dist folder is empty, and that the version number in setup.py has been incremenented
1. run `python setup.py sdist bdist_wheel`
2. run `python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/* --verbose`

you will need to get a token from: https://pypi.org/manage/account/token/


## notes for devs:
There is one annoying thing about *.gitignore**... it  will only effect files that haven't been 'added' already.

To make new .gitignore entries affect all files 

 1. Make changes to .gitignore
 2. `git commit -a -m "Pre .gitignore changes"`
 3. `git rm -r --cached .`
 4. `git add .`
 5. `git commit -a -m "Post .gitignore changes"`
 6. `git status`
 should output "nothing to commit (working directory clean)"
`# this is the readme!`
