from setuptools import setup, find_packages

setup(
	name='jb4jupyter',
	version='0.18',
	description='jupyter bettifier by @jb4earth',
	url='http://gitlab.com/jb4earth/jb4jupyter',
	author='@jb4earth',
	author_email='jb4earth@gmail.com',
	license='GPL',
	packages=find_packages(),
	long_description='Out of this world beautiful plotting and reports for jupyter, with dark mode ;)',
    long_description_content_type="text/markdown"
	)
